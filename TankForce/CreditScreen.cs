﻿/*
 * Ivan Lazcano Sindin
 * TankForce - CreditScreen.cs
 */
using System;

public class CreditScreen
{
    public void Run()
    {
        Image welcome = new Image("data/misc/welcome.png");
        Font font18 = new Font("data/fonts/Joystix.ttf", 24);
        SdlHardware.ClearScreen();
        SdlHardware.DrawHiddenImage(welcome, 0, 0);
        SdlHardware.WriteHiddenText("(Partial) Remake by Ivan Lazcano",
            20, 20,
            10, 10, 10,
            font18);
        SdlHardware.WriteHiddenText("R to Return",
            20, 60,
            10, 10, 10,
            font18);
        SdlHardware.ShowHiddenScreen();

        do
        {
            SdlHardware.Pause(100); // To avoid using 100% CPU
        }
        while (!SdlHardware.KeyPressed(SdlHardware.KEY_R));
    }
}

