﻿/*
 * Ivan Lazcano Sindin
 * TankForce - EndScreen.cs
 */
using System;
using System.IO;
public class EndScreen
{
    protected Score[] scores;
    private int countScores;
    bool end ;
    Image score;
    Font font24;

    public EndScreen()
    {
        end = false;
        score = new Image("data/misc/score.png");
        font24 = new Font("data/fonts/Joystix.ttf", 24);

        scores = new Score[10];
        countScores = 0;
        for (int i = 0; i < 10; i++)
        {
            scores[i] = new Score("Anon", i * 15);
        }
        Array.Sort(scores);
    }

    public void Show()
    {

        SdlHardware.ClearScreen();
        SdlHardware.DrawHiddenImage(score, 0, 0);

        for (int i = 0; i < 10; i++)
        {
            SdlHardware.WriteHiddenText(scores[i].GetName() 
                    +" -> "+scores[i].GetScore(),
                    300, (short)(200+(i*25)), 240, 240, 240, font24);
        }

        SdlHardware.WriteHiddenText("Press space to exit",
                    300, 600, 240, 240, 240, font24);
        SdlHardware.ShowHiddenScreen();
        do
        {
            if (SdlHardware.KeyPressed(SdlHardware.KEY_SPC))
            {
                end = true;
            }
        } while (!end);

    }

    public void Add(string name, int score)
    {
        Score aux = new Score(name, score);
        if (countScores < 9)
        {
            scores[countScores] = aux;
            countScores++;
        }
        else
        {
            if (scores[9].GetScore() < aux.GetScore())
                scores[countScores] = aux;
        }
        Array.Sort(scores);
    }

    public void Save()
    {
        StreamWriter file = new StreamWriter("scores.dat");
        foreach (Score s in scores)
        {
            file.WriteLine(s.GetName());
            file.WriteLine(s.GetScore());
        }
        file.Close();
    }

    public void Load()
    {
        if (File.Exists("scores.dat"))
        {
            int count = 0;
            StreamReader file = new StreamReader("scores.dat");
            string name = file.ReadLine();
            string score = file.ReadLine();

            while (name != null && count <= 10 && score != null)
            {
                scores[count] = new Score(name, Convert.ToInt32(score));
                name = file.ReadLine();
                score = file.ReadLine();
                count++;
            }
            file.Close();
        }
        Array.Sort(scores);
    }
}

