﻿/*
 * Ivan Lazcano Sindin
 * TankForce - Enemy.cs
 */
using System;

class Enemy : Sprite
{
    protected Shot s;
    protected bool IsDead;
    protected int steeps;
    private static Random rnd;
    protected int speed;
    protected int countRandom;

    public Enemy()
    {
        LoadImage("data/tanks/enemy_1_normal.png");
        width = 48;
        height = 48;
        s = new Shot("data/misc/disparo.png");
        IsDead = false;
        steeps = 0;
        rnd = new Random();
        speed = 4;
        countRandom = 4;

        LoadSequence(LEFT,
                     new string[] { "data/tanks/player_red.png",
            "data/tanks/player_red_luces_left.png"});
        LoadSequence(RIGHT,
                     new string[] { "data/tanks/player_red_luces_right.png",
            "data/tanks/player_red_right.png"});

        LoadSequence(DOWN,
                     new string[] { "data/tanks/player_red_bot2.png",
            "data/tanks/player_red_bot.png"});

        LoadSequence(UP,
                     new string[] { "data/tanks/player_red_luces_top.png",
            "data/tanks/player_red_top.png"});

    }

    public Shot GetShot() { return s; }

    public bool IsAlive()
    {
        return !IsDead;
    }

    public void Kill()
    {
        IsDead = true;
        visible = false;
    }

    public void Move(Room room, Player player)
    {
        steeps--;
        if (steeps <= 0)
        {
            countRandom--;

            if(countRandom <= 0)
            {//Go to player direction some times
                countRandom = 1;
                steeps = 35;
                xSpeed = x < player.GetX() ? 5 : -5;
                ySpeed = y < player.GetY() ? -5 : 5;
            }

            steeps = rnd.Next(20, 50);
            ySpeed = xSpeed = 0;
            if (rnd.Next(1, 10) < 5)
                xSpeed = rnd.Next(1, 10) < 5 ? -speed : speed;
            else
                ySpeed = rnd.Next(1, 10) < 5 ? -speed : speed;

            if(xSpeed < 0)
            {
                ChangeDirection(LEFT);
            }
            else if(xSpeed > 0)
                ChangeDirection(RIGHT);
            else if(ySpeed < 0)
                ChangeDirection(UP);
            else
                ChangeDirection(DOWN);




        }

        if (room.CanMoveTo(x + xSpeed, y, x + width + xSpeed,
                           y + height) && x+xSpeed > 0 && y+ySpeed > 0
            && x + xSpeed+ width < TankForce.SCREENWIDTH
                && y + ySpeed + height < TankForce.SCREENHEITH)
        {
            x += xSpeed;
            y += ySpeed;
        }
    }

    public void Shot()
    {
        s.Shots(currentDirection, x, y);
    }
}

