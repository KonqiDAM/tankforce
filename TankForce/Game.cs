﻿/*
 * Ivan Lazcano Sindin
 * TankForce - Game.cs
 */
using System;
using System.Collections.Generic;
class Game
{
    protected Player player;
    protected List<Enemy> enemies;
    protected int totalEnemies;
    protected int maxEnemiesOnScreen;
    protected bool finished;
    protected Font font18;
    protected Room room;
    protected int score;
    protected int lives;


    public int GetScore()
    {
        return score;
    }

    void Init()
    {
        score = 0;
        lives = 3;
        player = new Player();
        totalEnemies = 12;
        maxEnemiesOnScreen = 5;
        room = new Room();
        finished = false;

        Random rnd = new Random();
        enemies = new List<Enemy>();
        for (int i = 0; i < totalEnemies; i++)
        {
            enemies.Add(new Enemy());
            enemies[i].MoveTo(rnd.Next(200, 800), rnd.Next(50, 600));
            enemies[i].SetSpeed(rnd.Next(1, 5), rnd.Next(1, 5));

            //Creates an enemy and cheks if it can spawn (not inside an building)
            while (!room.CanMoveTo(enemies[i].GetX(), enemies[i].GetY(),
                           enemies[i].GetX() + enemies[i].GetWidth(),
                                  enemies[i].GetY() + enemies[i].GetHeight()))
            {
                enemies[i].MoveTo(rnd.Next(200, 800), rnd.Next(50, 600));
            }


            if (i < maxEnemiesOnScreen)
                enemies[i].Show();
            else
                enemies[i].Hide();

        }

        font18 = new Font("data/fonts/Joystix.ttf", 18);

    }

    void UpdateScreen()
    {
        
        SdlHardware.ClearScreen();
        room.DrawOnHiddenScreen();

        SdlHardware.WriteHiddenText("Score: "+score,
                40, (short)(TankForce.SCREENHEITH-20),
                0xCC, 0xCC, 0xCC, font18);

        SdlHardware.WriteHiddenText("Lives: " + lives,
                500, (short)(TankForce.SCREENHEITH - 20),
                0xCC, 0xCC, 0xCC, font18);

        player.DrawOnHiddenScreen();
        for (int i = 0; i < totalEnemies; i++)
        {
            //draw enemy shots
            enemies[i].DrawOnHiddenScreen();
            if (enemies[i].GetShot().IsVisible())
                enemies[i].GetShot().DrawOnHiddenScreen();
        }

        if(player.GetShot().IsVisible())
        {
            //Draw the shot from the player
            player.GetShot().DrawOnHiddenScreen();
        }
        SdlHardware.ShowHiddenScreen();
    }

    void CheckUserInput()
    {
        //TODO The move sould have the colision checker included
        if (SdlHardware.KeyPressed(SdlHardware.KEY_RIGHT))
        {
            if (room.CanMoveTo(player.GetX() + player.GetSpeedX(),
                    player.GetY(),
                    player.GetX() + player.GetWidth() + player.GetSpeedX(),
                    player.GetY() + player.GetHeight()))
            player.MoveRight();
        }
        else if (SdlHardware.KeyPressed(SdlHardware.KEY_LEFT))
        {
            if (room.CanMoveTo(player.GetX() - player.GetSpeedX(),
                    player.GetY(),
                    player.GetX() + player.GetWidth() - player.GetSpeedX(),
                    player.GetY() + player.GetHeight()))
            player.MoveLeft();
        }
        else if (SdlHardware.KeyPressed(SdlHardware.KEY_UP))
        {
            //TODO frist update the sprite to allow rotations when colliding with a wall
            //player.ChangeDirection(3);
            //player.NextFrame();
            if (room.CanMoveTo(player.GetX(),
                    player.GetY() - player.GetSpeedY(),
                    player.GetX() + player.GetWidth(),
                    player.GetY() + player.GetHeight() - player.GetSpeedY()))
            player.MoveUp();
        }
        else if (SdlHardware.KeyPressed(SdlHardware.KEY_DOWN))
        {
            if (room.CanMoveTo(player.GetX(),
                    player.GetY() + player.GetSpeedY(),
                    player.GetX() + player.GetWidth(),
                    player.GetY() + player.GetHeight() + player.GetSpeedY()))
            player.MoveDown();
        }
        if(SdlHardware.KeyPressed((SdlHardware.KEY_SPC)))
        {
            //We always shot, but in class shot we check if we can fire or not
            player.Shot();
        }
        if (SdlHardware.KeyPressed(SdlHardware.KEY_ESC))
            finished = true;
    }

    void UpdateWorld()
    {
        // Move enemies, background, etc 
        for (int i = 0; i < totalEnemies; i++)
        {
            enemies[i].Move(room, player);
            enemies[i].GetShot().Move();
        }
        player.GetShot().Move();
    }

    void CheckGameStatus()
    {
        // Check collisions and apply game logic
        for (int i = 0; i < totalEnemies; i++)
        {
            //End game if we are hit
            if (player.CollisionsWith(enemies[i]) 
                || enemies[i].GetShot().CollisionsWith(player))
            {
                enemies[i].GetShot().Hide();
                score = 0;
                lives--;
                if(lives <= 0)
                    finished = true;
            }
                
            //kill enemies hit by our shot
            if (player.GetShot().CollisionsWith(enemies[i]))
            {
                score += 200;
                enemies[i].Kill();
                player.GetShot().Hide();
                foreach(Enemy e in enemies)
                {
                    if (e.IsAlive() && (!e.IsVisible()))
                    {
                        e.Show();
                        break;
                    }                    
                }
            }

            //The enemy is always shooting, could add a cooldown
            if (enemies[i].IsVisible())
                enemies[i].Shot();
        }

        //Checks if the player shots is out of screen
        if(player.GetShot().GetX() < 0 || player.GetShot().GetX() > 
           SdlHardware.GetWidth()-player.GetShot().GetWidth() 
           || player.GetShot().GetY() < 0 || player.GetShot().GetY() >
           SdlHardware.GetHeight()-player.GetShot().GetHeight())
        {
            player.GetShot().Hide();
        }

        //Same for all enemies
        for (int i = 0; i < totalEnemies; i++)
        {
            if (enemies[i].GetShot().GetX() < 0 || enemies[i].GetShot().GetX()
                > SdlHardware.GetWidth() - enemies[i].GetShot().GetWidth()
                || enemies[i].GetShot().GetY() < 0 || enemies[i].GetShot().GetY()
                > SdlHardware.GetHeight() - enemies[i].GetShot().GetHeight())
            {
                enemies[i].GetShot().Hide();
            }
        }

    }

    void PauseUntilNextFrame()
    {
        SdlHardware.Pause(15);
    }

    void Score()
    {
        if(score > 0)
            score--;
    }

    void Pause()
    {
        SdlHardware.Pause(15);
    }

    public void Run()
    {
        Init();

        do
        {
            UpdateScreen();
            CheckUserInput();
            UpdateWorld();
            PauseUntilNextFrame();
            CheckGameStatus();
            Score();
        }
        while (!finished);

    }
}