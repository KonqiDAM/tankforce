﻿/*
 * Ivan Lazcano Sindin
 * TankForce - Player.cs
 */
using System;

class Player : Sprite
{
    protected Shot s;
    public Player()
    {
        LoadSequence(LEFT,
                     new string[] { "data/tanks/player_red_left.png",
            "data/tanks/player_red_luces_left.png"});
        LoadSequence(RIGHT,
                     new string[] { "data/tanks/player_red_right.png",
            "data/tanks/player_red_luces_right.png"});

        LoadSequence(DOWN,
                     new string[] { "data/tanks/player_red_bot.png",
            "data/tanks/player_red_luces_bot.png"}); 

        LoadSequence(UP,
                     new string[] { "data/tanks/player_red_top.png",
            "data/tanks/player_red_luces_top.png"}); 
        x = 48;
        y = 48;
        xSpeed = ySpeed = 4;
        width = 48;
        height = 48;

        currentDirection = LEFT;

        s = new Shot("data/misc/disparo_jugador.png");

    }

    public Shot GetShot() { return s; }

    public void MoveRight()
    {
        ChangeDirection(RIGHT);
        NextFrame();
        x += xSpeed;
    }
    public void MoveLeft()
    {
        ChangeDirection(LEFT);
        NextFrame();
        x -= xSpeed;
    }
    public void MoveUp()
    {
        ChangeDirection(UP);
        NextFrame();
        y -= ySpeed;
    }
    public void MoveDown()
    {
        ChangeDirection(DOWN);
        NextFrame();
        y += ySpeed;
    }

    public void Shot()
    {
        s.Shots(currentDirection, x, y);
    }


}

