﻿using System;

public class Score : IComparable<Score>
{
    protected string name;
    protected int score;

    public Score(string name, int score)
    {
        this.name = name;
        this.score = score;
    }

    public void SetName(string name) { this.name = name; }
    public string GetName() { return this.name; }

    public void SetScore(int score) { this.score = score; }
    public int GetScore() { return this.score; }

    public int CompareTo(Score obj)
    {
        return obj.GetScore() - this.GetScore();
    }
}