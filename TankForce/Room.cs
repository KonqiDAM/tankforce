﻿/*
 * Ivan Lazcano Sindin
 * TankForce - Room.cs
 */

class Room
{
    protected Image floor1, floor2, floor3, floor4, 
        floor5, floor6;
    protected Image building1, building2, building3, building4, building5,
    building6;
    protected Image box1, box2, box3, box4;
    protected int mapHeight = 28, mapWidth = 36;
    protected int tileWidth = 24, tileHeight = 24;
    protected int leftMargin = 0, topMargin = 0;

    protected string[] levelData =
    {
        "ABCDEFABCDEFABCDEFABCDEFABCDEFABCD  ",
        "ABCDEFABCDEFABCDEFABCDEFABCDEFABCD  ",
        "ABCDEFAB12EFABCDEFABCDEFABCDEFABCD  ",
        "ABCDEFAB34EFABCDEFABCDEFABCDEFABCD  ",
        "ABCDEFABCDEFABCDEFABCDEFABCDEFABCD  ",
        "ABCDEFABCDEFABCDEFABCDEFABCDEFABCD  ",
        "ABCDEFABCDEFABCDEFABCDEFABCDEFABCD  ",
        "ABCDEFABCDEFABCDEFABCDEFABCDEFABCD  ",
        "ABCDEFABCDEFABCDEFABCDEFABCDEFABCD  ",
        "ABCDEFABCDEFABCDEFABCDEFABCDEFABCD  ",
        "ABCDEFABCDEFABCDEFABCDEFABCDEFABCD  ",
        "ABCDEFABCDEFABCDEFABCDEFABCDEFABCD  ",
        "ABCDEFABCDEFABCDEFABCDEFABCDEFABCD  ",
        "ABCDEFABCDEFABCDEFABCDEFABCDEFABCD  ",
        "ABCDEFABCDEFABCDE12BCDEFABCDEFABCD  ",
        "ABCDEFABCDEFABCDE34BCDEFABCDEFABCD  ",
        "ABCDEFABCDEFABCDEFABCDEFABCDEFABCD  ",
        "ABCDEFABCDEFAB12EFABCDEFABCDEFABCD  ",
        "ABCDEFABCDEFAB34EFABCDEFABCDEFABCD  ",
        "ABCDEFABCDEFAB12EFABCDEFABCDEFABCD  ",
        "ABCDEFABCDEFAB34EFABCDEFABCDEFABCD  ",
        "ABCDEFABCDEFAB12EFABCDEFABCDEFABCD  ",
        "ABCDEFABCDEFAB34EFABCDEFLKCDEFABCD  ",
        "ABCDEFABCDEFAB12EFABCDEFJICDEFABCD  ",
        "ABCDEFABCDEFAB34EFABCDEFHGCDEFABCD  ",
        "ABCDEFABCDEFABCDEFABCDEFABCDEFABCD  ",
        "ABCDEFABCDEFABCDEFABCDEFABCDEFABCD  ",
        "                                    ",
    };

    public Room()
    {
        //TODO add more sprites and secuences
        floor1 = new Image("data/suelos/suelo3-1.png");//A
        floor2 = new Image("data/suelos/suelo3-2.png");//B
        floor3 = new Image("data/suelos/suelo3-3.png");//C
        floor4 = new Image("data/suelos/suelo3-4.png");//D
        floor5 = new Image("data/suelos/suelo3-5.png");//e
        floor6 = new Image("data/suelos/suelo3-6.png");//F

        box1 = new Image("data/edificios/cajaHor-top-izqu.png");//1
        box2 = new Image("data/edificios/cajaHor-top-derecha.png");//2
        box3 = new Image("data/edificios/cajaHor-bot-iz.png");//3
        box4 = new Image("data/edificios/cajaHor-bot-derecha.png");//4

        building1 = //G
            new Image("data/edificios/edificio_naranja_bot_derecha.png");
        building2 = //H
            new Image("data/edificios/edificio_naranja_bot_izquierda.png");
        building3 = //I
            new Image("data/edificios/edificio_naranja_mid_derecha.png");
        building4 = //J
            new Image("data/edificios/edificio_naranja_mid_izquierda.png");
        building5 = //K
            new Image("data/edificios/edificio_naranja_top_derecha.png");
        building6 = //L
            new Image("data/edificios/edificio_naranja_top_izquierda.png");
    }

    public void DrawOnHiddenScreen()
    {
        for (int row = 0; row < mapHeight; row++)
        {
            for (int col = 0; col < mapWidth; col++)
            {
                int posX = col * tileWidth + leftMargin;
                int posY = row * tileHeight + topMargin;
                switch (levelData[row][col])
                {
                    case 'A': SdlHardware.DrawHiddenImage(floor1, posX, posY); break;
                    case 'B': SdlHardware.DrawHiddenImage(floor2, posX, posY); break;
                    case 'C': SdlHardware.DrawHiddenImage(floor3, posX, posY); break;
                    case 'D': SdlHardware.DrawHiddenImage(floor4, posX, posY); break;
                    case 'E': SdlHardware.DrawHiddenImage(floor5, posX, posY); break;
                    case 'F': SdlHardware.DrawHiddenImage(floor6, posX, posY); break;
                    case '1': SdlHardware.DrawHiddenImage(box1, posX, posY); break;
                    case '2': SdlHardware.DrawHiddenImage(box2, posX, posY); break;
                    case '3': SdlHardware.DrawHiddenImage(box3, posX, posY); break;
                    case '4': SdlHardware.DrawHiddenImage(box4, posX, posY); break;
                    case 'G': SdlHardware.DrawHiddenImage(building1, posX, posY); break;
                    case 'H': SdlHardware.DrawHiddenImage(building2, posX, posY); break;
                    case 'I': SdlHardware.DrawHiddenImage(building3, posX, posY); break;
                    case 'J': SdlHardware.DrawHiddenImage(building4, posX, posY); break;
                    case 'K': SdlHardware.DrawHiddenImage(building5, posX, posY); break;
                    case 'L': SdlHardware.DrawHiddenImage(building6, posX, posY); break;
                        
                }
            }
        }
    }

    public bool CanMoveTo(int x1, int y1, int x2, int y2)
    {
        for (int column = 0; column < mapWidth; column++)
        {
            for (int row = 0; row < mapHeight; row++)
            {
                char tile = levelData[row][column];
                if (tile == '1' || tile == '2' || tile == '3' || tile == '4' ||
                    tile == 'G' || tile == 'H' || tile == 'I' || tile == 'K' ||
                    tile == 'J' ||
                    tile == 'L' 
                   //Colisions with tiles like boxes and buildings
                   )
                    // 
                {
                    int x1tile = leftMargin + column * tileWidth;
                    int y1tile = topMargin + row * tileHeight;
                    int x2tile = x1tile + tileWidth;
                    int y2tile = y1tile + tileHeight;
                    if ((x1tile < x2) &&
                        (x2tile > x1) &&
                        (y1tile < y2) &&
                        (y2tile > y1) // Collision as bouncing boxes
                        )
                        return false;
                }

            }
        }
        return true;
    }
}