﻿using System;

class Shot : Sprite
{
    public Shot(string img)
    {
        LoadImage(img);
        width = 18;
        height = 18;
        xSpeed = 0;
        ySpeed = 0;
        visible = false;
    }

    public void Shots(byte DIRECTION, int x, int y)
    {
        //Shots in the directionm of the sprite only if there isnt another one in the screen
        if (!visible)
        {
            this.x = x;
            this.y = y;
            xSpeed = 0;
            ySpeed = 0;
            visible = true;
            if (DIRECTION == RIGHT)
                ShotRight();
            else if (DIRECTION == LEFT)
                ShotLeft();
            else if (DIRECTION == UP)
                ShotUp();
            else
                ShotDown();
        }
    }

    private void ShotRight()
    {
        ChangeDirection(RIGHT);
        xSpeed = 10;
    }
    private void ShotLeft()
    {
        ChangeDirection(LEFT);
        xSpeed = -10;
    }
    private void ShotUp()
    {
        ChangeDirection(UP);
        ySpeed = -10;
    }
    private void ShotDown()
    {
        ChangeDirection(DOWN);
        ySpeed = 10;
    }

    public override void Move()
    {
        MoveTo(x+xSpeed,y+ySpeed);
    }
}

