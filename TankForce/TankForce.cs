﻿/*
 * Ivan Lazcano Sindin
 * TankForce - TankForce.cs
 */

using System;


class TankForce
{
    public static short SCREENWIDTH = 864;
    public const short SCREENHEITH = 672;

    static void Main()
    {
        bool fullScreen = false;
        //Original size 288x224, game will be x3
        SdlHardware.Init(SCREENWIDTH, SCREENHEITH, 24, fullScreen);
        //36 x 28
        WelcomeScreen w = new WelcomeScreen();
        EndScreen end = new EndScreen();

        do
        {
            w.Run();
            if (w.GetChosenOption() == 1)
            {
                Game g = new Game();

                g.Run();
                //end.Load();
                end.Add("new player", g.GetScore());
                //end.Save();
                end.Show();
                
            }
            else if (w.GetChosenOption() == 2)
            {
                CreditScreen credits = new CreditScreen();
                credits.Run();
            }
        }
        while (w.GetChosenOption() != 3);


    }
}
