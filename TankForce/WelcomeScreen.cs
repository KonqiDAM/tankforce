﻿/*
 * Ivan Lazcano Sindin
 * TankForce - WelcomeScreen.cs
 */
using System;

class WelcomeScreen
{
    protected Image welcome;
    protected int option;
    protected Font font24;

    public WelcomeScreen()
    {
        welcome = new Image("data/misc/welcome.png");
        option = 0;
        font24 = new Font("data/fonts/Joystix.ttf", 24);
    }

    public int GetChosenOption()
    {
        return option;
    }

    public void Run()
    {
        
        SdlHardware.ClearScreen();
        SdlHardware.DrawHiddenImage(welcome, 0, 0);
        SdlHardware.WriteHiddenText("1. Play",
            20, 20, 10, 10, 10, font24);
        SdlHardware.WriteHiddenText("2. Credits",
            20, 60,
            10, 10, 10,
            font24);
        SdlHardware.WriteHiddenText("Q. Quit",
            20, 100,
            10, 10, 10,
            font24);

        SdlHardware.ShowHiddenScreen();
        option = 0;
        do
        {
            if (SdlHardware.KeyPressed(SdlHardware.KEY_1))
            {
                option = 1;
            }
            if (SdlHardware.KeyPressed(SdlHardware.KEY_2))
            {
                option = 2;
            }
            if (SdlHardware.KeyPressed(SdlHardware.KEY_Q))
            {
                option = 3;
            }
            SdlHardware.Pause(100); // To avoid using 100% CPU
        }
        while (option == 0);
    }
}

